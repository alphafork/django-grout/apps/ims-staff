from django.core.exceptions import ValidationError
from ims_base.apis import BaseAPIViewSet
from ims_user.apis import UserFilter
from rest_framework.filters import BaseFilterBackend

from ims_staff.serializers import (
    StaffDesignationSerializer,
    StaffRegistrationSerializer,
    StaffSerializer,
)

from .models import Staff, StaffDesignation, StaffUser


class StaffUserFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "user" in request.query_params:
            model_name = view.model_class._meta.model_name
            if model_name == "staff":
                return queryset.filter(user=request.query_params["user"])
            if model_name == "staffdesignation":
                return queryset.filter(staff__user=request.query_params["user"])
        return queryset


class StaffIDFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "staffid" in request.query_params:
            model_name = view.model_class._meta.model_name
            if model_name == "staff":
                return queryset.filter(staffid=request.query_params["staffid"])
            if model_name == "staffdesignation":
                return queryset.filter(staff__staffid=request.query_params["staffid"])
        return queryset


class DesignationFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "designation" in request.query_params:
            filter_map.update(
                {
                    "designation__name": request.query_params["designation"],
                }
            )
        for filter_str in ["designation_id", "is_active", "is_permanent", "is_default"]:
            if filter_str in request.query_params:
                filter_map.update(
                    {
                        filter_str: request.query_params[filter_str],
                    }
                )

        try:
            return queryset.filter(**filter_map)
        except (ValueError, ValidationError):
            return []


class StaffAPI(BaseAPIViewSet):
    serializer_class = StaffSerializer
    model_class = Staff
    filter_backends = BaseAPIViewSet.filter_backends + [StaffIDFilter, StaffUserFilter]


class StaffDesignationAPI(BaseAPIViewSet):
    serializer_class = StaffDesignationSerializer
    model_class = StaffDesignation
    filter_backends = BaseAPIViewSet.filter_backends + [
        StaffUserFilter,
        StaffIDFilter,
        DesignationFilter,
        UserFilter,
    ]
    user_lookup_field = "staff__user"
    obj_user_groups = ["Admin", "Accountant"]


class StaffRegistrationAPI(BaseAPIViewSet):
    serializer_class = StaffRegistrationSerializer
    model_class = StaffUser

    def perform_create(self, serializer):
        user = serializer.save(self.request)
        return user
