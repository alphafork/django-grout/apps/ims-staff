from django.contrib.auth.models import User
from django.db.models import Q
from ims_base.serializers import BaseModelSerializer
from ims_user.serializers import UserRegistrationSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from reusable_models import get_model_from_string

from ims_staff.services import staffid_generator

from .models import (
    Designation,
    PermanentStaffDesignation,
    Staff,
    StaffDesignation,
    TemporaryStaffDesignation,
)

Contact = get_model_from_string("CONTACT")
Gender = get_model_from_string("GENDER")
Honorific = get_model_from_string("HONORIFIC")
Profile = get_model_from_string("PROFILE")


class StaffSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["staffid"]
        extra_meta = {
            "user": {
                "related_model_url": "/user/account",
            }
        }

    def create(self, validated_data):
        validated_data["staffid"] = staffid_generator()
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["name"] = instance.user.get_full_name()
        data["staffid"] = instance.staffid
        return data


class StaffDesignationSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        read_only_fields = ["staffid"]
        read_only_extra_meta = {
            "email": {
                "type": "string",
                "label": "Email",
            },
        }

    def get_read_only_extra_meta(self):
        return self.Meta.read_only_extra_meta

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        designation = validated_data.get("designation")
        staff = validated_data.get("staff")
        report_date = validated_data.get("report_date", None)
        relieve_date = validated_data.get("relieve_date", None)
        filter_map = {}
        if report_date:
            filter_map.update({"report_date__lte": report_date})
            if relieve_date:
                filter_map.update({"relieve_date__gte": relieve_date})
        staff_designation = StaffDesignation.objects.filter(
            staff=staff, designation=designation
        ).filter(Q(**filter_map) | Q(report_date=None, relieve_date=None))
        if staff_designation.exists():
            if (
                self.instance
                and staff_designation.count() == 1
                and staff_designation.get().id == self.instance.id
            ):
                pass
            else:
                raise ValidationError(
                    {"designation": "Staff is already active in the designation."}
                )
        if (
            self.instance
            and not self.context["request"].user.groups.filter(name="Admin")
            and not self.context["request"].user.is_superuser
        ):
            for field in [
                "staff",
                "designation",
                "report_date",
                "relieve_date",
                "is_active",
                "is_permanent",
            ]:
                if getattr(self.instance, field, None) != validated_data[field]:
                    raise ValidationError(
                        {field: "You are not allowed to update this field."}
                    )
        is_default = validated_data["is_default"]
        is_active = validated_data["is_active"]
        default_designation = StaffDesignation.objects.filter(
            is_default=True,
            staff=validated_data["staff"],
        )
        if self.instance:
            default_designation = default_designation.exclude(pk=self.instance.id)
        if is_active and not is_default and not default_designation.exists():
            raise ValidationError(
                {
                    "is_default": (
                        "At least one staff designation should be default."
                        "Make a designation default."
                    )
                }
            )
        return validated_data

    def update(self, instance, validated_data):
        if (
            not validated_data["is_active"]
            and instance.is_active
            and instance.is_default
        ):
            active_designations = StaffDesignation.objects.filter(
                is_default=False,
                is_active=True,
                staff=validated_data["staff"],
            )
            if active_designations.exists():
                active_designation = active_designations.first()
                active_designation.is_default = True
                active_designation.save()
            validated_data["is_default"] = False
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["name"] = instance.staff.user.get_full_name()
        data["designation_name"] = instance.designation.__str__()
        data["email"] = instance.staff.user.email
        return data


class PermanentStaffDesignationSerializer(StaffDesignationSerializer):
    class Meta(StaffDesignationSerializer.Meta):
        excluded_fields = StaffDesignationSerializer.Meta.excluded_fields + [
            "is_permanent"
        ]
        model = PermanentStaffDesignation

    def create(self, validated_data):
        validated_data["is_permanent"] = True
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["is_permanent"] = instance.is_permanent
        return data


class TemporaryStaffDesignationSerializer(StaffDesignationSerializer):
    class Meta(StaffDesignationSerializer.Meta):
        excluded_fields = StaffDesignationSerializer.Meta.excluded_fields + [
            "is_permanent"
        ]
        model = TemporaryStaffDesignation

    def create(self, validated_data):
        validated_data["is_permanent"] = False
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["is_permanent"] = instance.is_permanent
        return data


class StaffRegistrationSerializer(UserRegistrationSerializer):
    designation = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Designation.objects.all()
    )
    report_date = serializers.DateField(write_only=True, required=False)
    is_permanent = serializers.BooleanField(write_only=True)

    class Meta(UserRegistrationSerializer.Meta):
        fields = [
            "designation",
            "report_date",
            "is_permanent",
            "gender",
            "honorific",
            "date_of_birth",
            "phone_no",
            "current_address",
            "current_postalcode",
            "password1",
            "password2",
        ]
        UserRegistrationSerializer.Meta.extra_meta.update(
            {
                "designation": {
                    "related_model_url": "/staff/designation",
                },
            },
        )

    def save(self, request):
        designation = self.validated_data.pop("designation", None)
        report_date = self.validated_data.pop("report_date", None)
        is_permanent = self.validated_data.pop("is_permanent", None)

        user = super().save(request)
        staff = Staff.objects.create(
            user=user,
            staffid=staffid_generator(),
        )
        StaffDesignation.objects.create(
            staff=staff,
            designation=designation,
            report_date=report_date,
            is_active=True,
            is_permanent=is_permanent,
            is_default=True,
        )
        return user

    def to_representation(self, instance):
        data = super().to_representation(instance)
        user = User.objects.get(email=dict(data)["email"])
        staff = Staff.objects.filter(user=user).last()
        staff_designation = staff.staffdesignation_set.last()
        data["designation"] = staff_designation.designation.name
        data["staff ID"] = staff.staffid
        data["name"] = user.get_full_name()
        return data
