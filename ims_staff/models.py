from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail, AbstractLog


class Designation(AbstractBaseDetail):
    user_groups = models.ManyToManyField(Group)


class Staff(AbstractLog):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    staffid = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return "%s | %s" % (self.user.get_full_name(), self.staffid)


class StaffDesignation(AbstractLog):
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE)
    designation = models.ForeignKey(Designation, on_delete=models.CASCADE)
    report_date = models.DateField(null=True, blank=True)
    relieve_date = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    is_default = models.BooleanField()
    is_permanent = models.BooleanField()

    def __str__(self):
        return "%s | %s" % (self.staff, self.designation)


class StaffUserManager(models.Manager):
    def get_queryset(self):
        staff_user_list = Staff.objects.all().values_list("user", flat=True)
        return super().get_queryset().filter(id__in=staff_user_list)


class StaffUser(User):
    objects = StaffUserManager()

    class Meta:
        proxy = True


class PermanentStaffDesignationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_permanent=True)


class PermanentStaffDesignation(StaffDesignation):
    objects = PermanentStaffDesignationManager()

    class Meta:
        proxy = True


class TemporaryStaffDesignationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_permanent=False)


class TemporaryStaffDesignation(StaffDesignation):
    objects = TemporaryStaffDesignationManager()

    class Meta:
        proxy = True


class LeaveType(AbstractBaseDetail):
    pass


class Leave(AbstractLog):
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE)
    leave_from = models.DateField(null=True, blank=True)
    leave_to = models.DateField(null=True, blank=True)
    day_count = models.PositiveSmallIntegerField()
    leave_type = models.ForeignKey(LeaveType, on_delete=models.CASCADE)
    remarks = models.TextField(blank=True)

    def __str__(self):
        return self.staff.__str__()


def add_user_groups(user, designation):
    groups = designation.user_groups.all()
    for group in groups:
        user.groups.add(group)


def remove_user_groups(user, designation):
    designation_user_groups = list(designation.user_groups.all())
    staff_roles_except_current = StaffDesignation.objects.filter(
        staff__user=user
    ).exclude(designation=designation)
    for staff in staff_roles_except_current:
        staff_designation = staff.designation
        for user_group in designation_user_groups:
            if user_group not in staff_designation.user_groups.all():
                user.groups.remove(user_group)


@receiver(post_save, sender=Staff)
def add_to_staff_group(instance, created, **kwargs):
    if created:
        staff_group = Group.objects.get(name="Staff")
        instance.user.groups.add(staff_group)


@receiver(post_save, sender=StaffDesignation)
@receiver(post_save, sender=PermanentStaffDesignation)
@receiver(post_save, sender=TemporaryStaffDesignation)
def update_staff_user_groups(instance, **kwargs):
    if instance.is_active:
        add_user_groups(instance.staff.user, instance.designation)
    else:
        remove_user_groups(instance.staff.user, instance.designation)


@receiver(pre_delete, sender=StaffDesignation)
def remove_user_groups_on_staff_delete(instance, **kwargs):
    remove_user_groups(instance.staff.user, instance.designation)


@receiver(pre_save, sender=StaffDesignation)
@receiver(pre_save, sender=PermanentStaffDesignation)
@receiver(pre_save, sender=TemporaryStaffDesignation)
def update_default_staff_role(instance, **kwargs):
    if instance.is_default:
        staff_designations = StaffDesignation.objects.filter(
            is_default=True,
            staff=instance.staff,
        )
        for staff_designation in staff_designations:
            staff_designation.is_default = False
            staff_designation.save()
