# Generated by Django 4.1.9 on 2024-01-14 16:56

from django.conf import settings
from django.db import migrations


def load_staff_group(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.get_or_create(name="Staff")


class Migration(migrations.Migration):
    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("ims_staff", "0010_staff_designation"),
    ]

    operations = [
        migrations.RunPython(load_staff_group, reverse_code=migrations.RunPython.noop),
    ]
