# Generated by Django 4.1.9 on 2023-11-21 08:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ims_staff", "0006_update_defaultlogin_primarykey"),
    ]

    operations = [
        migrations.AddField(
            model_name="staff",
            name="is_default",
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name="StaffDefaultLogin",
        ),
    ]
