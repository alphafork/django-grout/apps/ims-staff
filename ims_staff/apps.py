from django.apps import AppConfig


class IMSStaffConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_staff"

    model_strings = {
        "STAFF": "Staff",
        "STAFF_DESIGNATION": "StaffDesignation",
        "DESIGNATION": "Designation",
        "LEAVETYPE": "LeaveType",
        "LEAVE": "Leave",
    }
